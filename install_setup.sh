# auto-setup script for devbox
# version: 1.1
#
# Installs and configures the following:
# - docker (docker-ce, docker-ce-cli, containerd.io, docker-buildx-plugin, docker-compose-plugin)
# - vscodium
# - vscode
# - brave
# - freecad
# - fortinet vpn
# - pyenv (python 3.9.15, 3.11.6)
# - poetry

sudo apt update && sudo apt upgrade -y && sudo apt autoremove -y dirmngr software-properties-common

# AI4CE DevOps things
sudo apt install -y git neovim htop btop open-vm-tools
echo 'alias ll="ls -lAh " ' >> ~/.bashrc
echo 'alias sv="source .venv/bin/activate " ' >> ~/.bashrc


# dev dependencies
sudo apt install -y wget gpg libssl-dev libbz2-dev libsqlite3-dev libreadline-dev liblzma-dev build-essential zlib1g-dev llvm libncurses-dev libncursesw-dev \
xz-utils tk-dev libffi-dev python-tk python3-tk \
ca-certificates curl gnupg

# install docker
# sudo apt install -y docker docker-compose

# Add Docker's official GPG key:
sudo apt-get update
sudo apt-get install ca-certificates curl
sudo install -m 0755 -d /etc/apt/keyrings
sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg -o /etc/apt/keyrings/docker.asc
sudo chmod a+r /etc/apt/keyrings/docker.asc

# Add the repository to Apt sources:
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/ubuntu \
  $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update

sudo apt-get install -y docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin


sudo groupadd docker
sudo usermod -aG docker $USER


# install vscodium
sudo wget https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/master/pub.gpg -O /usr/share/keyrings/vscodium-archive-keyring.asc
echo 'deb [ signed-by=/usr/share/keyrings/vscodium-archive-keyring.asc ] https://paulcarroty.gitlab.io/vscodium-deb-rpm-repo/debs vscodium main' \
    | sudo tee /etc/apt/sources.list.d/vscodium.list
sudo apt update && sudo apt install -y codium # or codium-insiders


# install vscode
wget -qO- https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > packages.microsoft.gpg
sudo install -D -o root -g root -m 644 packages.microsoft.gpg /etc/apt/keyrings/packages.microsoft.gpg
sudo sh -c 'echo "deb [arch=amd64,arm64,armhf signed-by=/etc/apt/keyrings/packages.microsoft.gpg] https://packages.microsoft.com/repos/code stable main" > /etc/apt/sources.list.d/vscode.list'
rm -f packages.microsoft.gpg
sudo apt update && sudo apt install -y code # or code-insiders

# install brave
sudo curl -fsSLo /usr/share/keyrings/brave-browser-archive-keyring.gpg https://brave-browser-apt-release.s3.brave.com/brave-browser-archive-keyring.gpg
echo "deb [signed-by=/usr/share/keyrings/brave-browser-archive-keyring.gpg] https://brave-browser-apt-release.s3.brave.com/ stable main"| sudo tee /etc/apt/sources.list.d/brave-browser-release.list
sudo apt update && sudo apt install -y brave-browser

# install freecad
sudo apt install -y freecad

# install fortinet vpn for connection to the CELab
wget https://links.fortinet.com/forticlient/deb/vpnagent
sudo chmod +x vpnagent
mv vpnagent vpnagent.deb
sudo apt install -y ./vpnagent.deb


# install pyenv
curl https://pyenv.run | bash
echo 'export PYENV_ROOT="$HOME/.pyenv"
command -v pyenv >/dev/null || export PATH="$PYENV_ROOT/bin:$PATH"
eval "$(pyenv init -)"
 ' >> ~/.bashrc 

# poetry install and setup
curl -sSL https://install.python-poetry.org | python3 -
poetry completions bash >> ~/.bash_completion
echo 'export PATH="$HOME/.local/bin:$PATH"' >> ~/.bashrc
# exec "$SHELL" # restart shell
source ~/.bashrc
poetry config virtualenvs.in-project true # dev env setup


# install python versions
pyenv update
# pyenv install 3.9.15
pyenv install 3.11.8
pyenv global 3.11.8

# Cleanup
sudo apt autoremove -y
sudo apt clean -y